## Open Questions

* In 3-e-link chips, how do the register read responses get returned?  In one e-link, in all 3?

* What is the maximum number of chips on a command link?

* Can the chips per command link be derived from this link? https://ghugo.web.cern.ch/ghugo/layouts/cabling/OT616_IT613_CMSSW_Ids/cablingInner.html#cabling

## Answered Questions
