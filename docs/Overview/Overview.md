#FW Block overview
![overview](it-dtc-fw-figs/output/Overview.png)

This is the top level block diagram for the IT-DTC FW.
Data flow is generally left to right going from the RD53B chips connected via LPGBTs to LPBGT-FPGA cores to DAQ via the slink rocket FW cores. 
