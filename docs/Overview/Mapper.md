# Mapping overview
The datapath from the RD53 chips to the processing in the FPGA goes through many merges and splits.
Chips can be merged, or split their data between e-links and then these are merged with other chip streams into the LPGBT datastream. 
These chips are also grouped by which command link they receive their trigger/register data from and the firmware reflects this grouping. 

To deal with this, the firmware first breaks up the incomming data into LPGBT cores.
The downlink of this has 1-5 command links and 1-6 uplinks with each uplink having 1/3 to 4 chips of data. 


# LPGBT grouping
![LPGBT Grouping](it-dtc-fw-figs/output/LPGBT_Grouping.png)

# Grouping by Command downlinks
![CMD link mapping](it-dtc-fw-figs/output/CMD_link_mapper.png)

# Mapping e-links from RD53B Chips
![Chip mapper](it-dtc-fw-figs/output/Chip_mapper.png)


# Blocks

### Trigger & Tag Management
This block takes in triggers from the TCDS system and builds the triggering data that gets sent down to the RD53B chip.
[TagManager.vhd](https://gitlab.com/apollo-lhc/it-dtc/it-dtc-fw/-/blob/develop/src/FE/TagManager/TagManager.vhd)
![Trigger & Tag Management](it-dtc-fw-figs//output/Tag_Manager.png)

### Register Interface
This block is an interface between the AXI(?) bus and the HDL needed to format register read/write messages to be sent down to the RD53B chip.

### Downlink Builder
This block takes in the fast trigger commands from the Trigger & Tag block and then register read/write commands from the Register Interface block and combines them and builds the serial stream that goes down to the RD53B chip(s). **Question: Is there a downlink per chip, or per module?** 
